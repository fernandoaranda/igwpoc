package com.example.igwapi.infrastructure;

import com.example.igwapi.domain.CompanyRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

public class KafkaCompanyProducer {
  public void requestCompanyUpdate(CompanyRequest companyRequest) {
    final Properties properties = new Properties();
    properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
    properties.setProperty(
        ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
    properties.setProperty(
        ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

    final KafkaProducer<String, String> producer = new KafkaProducer<>(properties);

    final ProducerRecord<String, String> record =
        new ProducerRecord<>("company_request", toJson(companyRequest));

    producer.send(record);

    producer.close();
    producer.flush();
  }

  public String toJson(CompanyRequest companyRequest) {
    final ObjectMapper objectMapper = new ObjectMapper();
    try {
      return objectMapper.writeValueAsString(companyRequest);
    } catch (JsonProcessingException e) {
      throw new RuntimeException();
    }
  }
}
