package com.example.apicustomer.infrastructure;

import com.example.igwapi.application.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/companies")
public class CompanyController {
  @Autowired private CompanyService companyService;

  @GetMapping("/{id}")
  public ResponseEntity requestCompanyUpdate(@PathVariable int id) {
    companyService.requestCompanyUpdate(id);

    return ResponseEntity.ok().build();
  }
}
