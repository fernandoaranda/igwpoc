package com.example.igwapi.application;

import com.example.igwapi.domain.Company;
import com.example.igwapi.domain.CompanyRequest;
import com.example.igwapi.infrastructure.KafkaCompanyProducer;
import org.springframework.stereotype.Service;

@Service
public class CompanyService {
  public void requestCompanyUpdate(int id) {
    final KafkaCompanyProducer kafkaCompanyProducer = new KafkaCompanyProducer();

    kafkaCompanyProducer.requestCompanyUpdate(new CompanyRequest(id, getProvider(id)));
  }

  private String getProvider(int id) {
    if (id > 100) {
      return "iberinform";
    } else {
      return "atradius";
    }
  }

  public Company buildCompany(String id) {
    return new Company(
        id, "Empresa retornada desde API", "Spain", "962865937", "Gandia", "Superprovider");
  }
}
