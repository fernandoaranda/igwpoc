package com.example.igwapi.domain;

import java.util.Date;

public class Company {
  private String id;
  private String name;
  private String country;
  private String phone;
  private String town;
  private String provider;
  private Date updatedOn;

  public Company() {}

  public Company(
      String id, String name, String country, String phone, String town, String provider) {
    this.id = id;
    this.name = name;
    this.country = country;
    this.phone = phone;
    this.town = town;
    this.provider = provider;
    this.updatedOn = new Date();
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getTown() {
    return town;
  }

  public void setTown(String town) {
    this.town = town;
  }

  public String getProvider() {
    return provider;
  }

  public void setProvider(String provider) {
    this.provider = provider;
  }

  public Date getUpdatedOn() {
    return updatedOn;
  }

  public void setUpdatedOn(Date updatedOn) {
    this.updatedOn = updatedOn;
  }

  @Override
  public String toString() {
    return "Company{"
        + "id='"
        + id
        + '\''
        + ", name='"
        + name
        + '\''
        + ", country='"
        + country
        + '\''
        + ", phone='"
        + phone
        + '\''
        + ", town='"
        + town
        + '\''
        + ", provider='"
        + provider
        + '\''
        + ", updatedOn="
        + updatedOn
        + '}';
  }
}
