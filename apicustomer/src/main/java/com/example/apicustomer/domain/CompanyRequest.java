package com.example.igwapi.domain;

public class CompanyRequest {
  private int id;
  private String provider;

  public CompanyRequest() {}

  public CompanyRequest(int id, String provider) {
    this.id = id;
    this.provider = provider;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getProvider() {
    return provider;
  }

  public void setProvider(String provider) {
    this.provider = provider;
  }
}
