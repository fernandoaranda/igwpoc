package com.example.apisuperprovider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/companies")
public class CompanyController {
  @Autowired CompanyService companyService;

  @GetMapping("/{id}")
  public ResponseEntity<Company> callbackFromCamel(@PathVariable String id) {
    return ResponseEntity.ok(companyService.buildCompany(id));
  }
}
