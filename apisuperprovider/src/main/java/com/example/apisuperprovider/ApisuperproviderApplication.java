package com.example.apisuperprovider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApisuperproviderApplication {

  public static void main(String[] args) {
    SpringApplication.run(ApisuperproviderApplication.class, args);
  }
}
