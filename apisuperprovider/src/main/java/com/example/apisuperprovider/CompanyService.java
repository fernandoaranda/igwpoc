package com.example.apisuperprovider;

import org.springframework.stereotype.Service;

@Service
public class CompanyService {
  public Company buildCompany(String id) {
    return new Company(
        id,
        "Empresa desde Superprovider",
        "+34",
        "962865937",
        "Av Constitucion 38",
        "Castelldefels",
        "Barcelona",
        "Spain",
        543);
  }
}
