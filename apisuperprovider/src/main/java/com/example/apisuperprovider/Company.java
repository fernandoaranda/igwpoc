package com.example.apisuperprovider;

public class Company {
  private String id;
  private String name;
  private String phoneCode;
  private String phoneNumber;
  private String street;
  private String level1;
  private String level2;
  private String level3;
  private Integer employees;

  public Company() {}

  public Company(
      String id,
      String name,
      String phoneCode,
      String phoneNumber,
      String street,
      String level1,
      String level2,
      String level3,
      Integer employees) {
    this.id = id;
    this.name = name;
    this.phoneCode = phoneCode;
    this.phoneNumber = phoneNumber;
    this.street = street;
    this.level1 = level1;
    this.level2 = level2;
    this.level3 = level3;
    this.employees = employees;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPhoneCode() {
    return phoneCode;
  }

  public void setPhoneCode(String phoneCode) {
    this.phoneCode = phoneCode;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getLevel1() {
    return level1;
  }

  public void setLevel1(String level1) {
    this.level1 = level1;
  }

  public String getLevel2() {
    return level2;
  }

  public void setLevel2(String level2) {
    this.level2 = level2;
  }

  public String getLevel3() {
    return level3;
  }

  public void setLevel3(String level3) {
    this.level3 = level3;
  }

  public Integer getEmployees() {
    return employees;
  }

  public void setEmployees(Integer employees) {
    this.employees = employees;
  }
}
