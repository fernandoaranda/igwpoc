package com.example.camel.infrastructure.atradius;

import com.example.camel.domain.Company;
import org.apache.camel.Converter;
import org.apache.camel.TypeConverters;
import org.springframework.stereotype.Component;

@Component
public class AtradiusCompanyConverter implements TypeConverters {
  @Converter
  public static Company toCompany(AtradiusCompany atradiusCompany) {
    final String capitalizedName =
        atradiusCompany.getName().substring(0, 1)
            + atradiusCompany.getName().toLowerCase().substring(1);
    final String capitalizedCountry =
        atradiusCompany.getCountry().substring(0, 1)
            + atradiusCompany.getCountry().toLowerCase().substring(1);

    return new Company(
        atradiusCompany.getId(),
        capitalizedName,
        capitalizedCountry,
        atradiusCompany.getPhone(),
        atradiusCompany.getTown(),
        "atradius");
  }
}
