package com.example.camel.infrastructure.iberinform;

import com.example.camel.domain.Company;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat;
import org.springframework.stereotype.Component;

@Component
public class IberinformRoute extends RouteBuilder {
  final String SOURCE_INPUT_PATH =
      "/Users/fernandoaranda/Downloads/iberinform?noop=true&include=.*\\.csv";

  @Override
  public void configure() {
    from("file:" + SOURCE_INPUT_PATH)
        .unmarshal(new BindyCsvDataFormat(IberinformCompany.class))
        .split(body())
        .convertBodyTo(Company.class)
        .to("kafka:new_company?brokers=localhost:9092");
  }
}
