package com.example.camel.infrastructure.atradius;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

@CsvRecord(separator = ";", skipField = true, skipFirstLine = true)
public class AtradiusCompany {
  @DataField(pos = 1)
  private String id;

  @DataField(pos = 2)
  private String name;

  @DataField(pos = 3)
  private String country;

  @DataField(pos = 4)
  private String phone;

  @DataField(pos = 5)
  private String address;

  @DataField(pos = 6)
  private String town;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getTown() {
    return town;
  }

  public void setTown(String town) {
    this.town = town;
  }
}
