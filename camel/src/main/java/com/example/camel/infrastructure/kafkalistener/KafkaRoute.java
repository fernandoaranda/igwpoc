package com.example.camel.infrastructure.kafkalistener;

import com.example.camel.domain.Company;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.springframework.stereotype.Component;

@Component
public class KafkaRoute extends RouteBuilder {
  @Override
  public void configure() throws Exception {
    restConfiguration().host("localhost").port(8090);
    final ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(new JSR310Module());

    from("kafka:company_request?brokers=localhost:9092")
        .to("rest:get:/companies/camel/543")
        .unmarshal(new JacksonDataFormat(Company.class))
        .to("kafka:new_company?brokers=localhost:9092");
  }
}
