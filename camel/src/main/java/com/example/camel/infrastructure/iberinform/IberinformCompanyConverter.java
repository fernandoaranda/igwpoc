package com.example.camel.infrastructure.iberinform;

import com.example.camel.domain.Company;
import org.apache.camel.Converter;
import org.apache.camel.TypeConverters;
import org.springframework.stereotype.Component;

@Component
public class IberinformCompanyConverter implements TypeConverters {
  @Converter
  public static Company toCompany(IberinformCompany iberinformCompany) {
    final String phone =
        CountryPhoneCodes.valueOf(iberinformCompany.getCountry().toUpperCase()).getCode()
            + iberinformCompany.getPhone();

    return new Company(
        iberinformCompany.getId(),
        iberinformCompany.getName(),
        iberinformCompany.getCountry(),
        phone,
        iberinformCompany.getCity(),
        "iberinform");
  }
}
