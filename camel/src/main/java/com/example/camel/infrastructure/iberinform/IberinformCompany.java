package com.example.camel.infrastructure.iberinform;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

import java.io.Serializable;

@CsvRecord(separator = ";", skipField = true, skipFirstLine = true)
public class IberinformCompany implements Serializable {
  @DataField(pos = 1)
  private String id;

  @DataField(pos = 2)
  private String name;

  @DataField(pos = 3)
  private String country;

  @DataField(pos = 4)
  private String phone;

  @DataField(pos = 5)
  private String city;

  @DataField(pos = 6)
  private String postCode;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getPostCode() {
    return postCode;
  }

  public void setPostCode(String postCode) {
    this.postCode = postCode;
  }
}
