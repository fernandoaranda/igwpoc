package com.example.camel.infrastructure.iberinform;

public enum CountryPhoneCodes {
  SPAIN("+34"),
  PORTUGAL("+99");

  private final String code;

  CountryPhoneCodes(String code) {
    this.code = code;
  }

  public String getCode() {
    return code;
  }
}
